#include "IRremote.h"
#include "FastLED.h"

#define NUM_LEDS 15
#define LED_DATA_PIN 5
#define IR_DATA_PIN 3

unsigned char led_brightness;
unsigned char r, g, b;
CRGB leds[NUM_LEDS];
IRrecv ir_recv(IR_DATA_PIN);
decode_results ir_recv_value;

void setup() 
{
  Serial.begin(9600);
  FastLED.addLeds<WS2812B, LED_DATA_PIN, GRB>(leds, NUM_LEDS);
  ir_recv.enableIRIn();
  r=255;g=255;b = 255;
  led_brightness = 255;
  show_leds();
}
 
void loop()
{
  if (ir_recv.decode(&ir_recv_value)) {
    translate_IR();
    ir_recv.resume();
  }
}

void translate_IR() {
  switch(ir_recv_value.value) {
    case 16716015: Serial.println("G UP"); g+= 10; show_RGB(r, g, b); break;
    case 16722135: Serial.println("CYAN"); r=0;g=128;b=128; show_RGB(r, g, b); break;
    case 16724175: Serial.println("G DOWN"); g-= 10; show_RGB(r, g, b);break;
    case 16732335: Serial.println("B UP"); b+=10;  show_RGB(r, g, b); break;
    case 16736415: Serial.println("OFF"); FastLED.clear(); LEDS.show(); break;
    case 16738455: Serial.println("PURPLE"); r=75;g=0;b=130; show_RGB(r, g, b); break;
    case 16740495: Serial.println("B DOWN"); b-= 10; show_RGB(r, g, b); break;
    case 16748655: Serial.println("R UP"); r+= 10; show_RGB(r, g, b); break;
    case 16754775: Serial.println("ORANGE"); r=255;g=100;b=0; show_RGB(r, g, b); break;
    case 16756815: Serial.println("R DOWN"); r-= 10; show_RGB(r, g, b);break;
    case 16764975: Serial.println("WHITE"); r=255;g=255;b=255; show_RGB(r, g, b);break;
    case 16769055: Serial.println("ON"); show_leds(); break;
    default: Serial.print("Unregistered button: "); Serial.println(ir_recv_value.value); break;
  }
}

void show_RGB(unsigned char red, unsigned char green, unsigned char blue) {
  for (int i = 0; i <= NUM_LEDS; i++)
  {
    leds[i] = CRGB(red, green, blue); 
    LEDS.show();
  }
}

void show_leds() {
  Serial.println(led_brightness);
  LEDS.setBrightness(led_brightness);
  show_RGB(r, g, b);
}
